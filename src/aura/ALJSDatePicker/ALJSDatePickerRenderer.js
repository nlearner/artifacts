({
	rerender : function(component, helper){
        this.superRerender();
        if(component.get("v.isJqueryLoaded") && component.get("v.needToProcessReRenderLogic")) {
            //your logics
            console.log('==rerender sobjlookup===');
			helper.initializeDatePicker(component, helper);
            console.log('==rerender sobjlookup==2=');
            //Finally set the needToProcessReRenderLogic to false, since rerender will be called multiple times
            component.set("v.needToProcessReRenderLogic",false); // this will not fire rerender again
        }

        if(component.get("v.needToProcessValidation"))
        {
            helper.validateInput(component);
            component.set("v.needToProcessValidation", false);
        }
    }
})