({
    addResource : function(component, event, helper) {
        component.set("v.isJqueryLoaded", true);
        component.set("v.needToProcessReRenderLogic", true);
    },
    
    validateDate: function(component, event, helper) {
		component.set("v.needToProcessValidation", true);
    },
    
    handlerError : function(component, event, helper)
    {
        if(component.get("v.dateId") == event.getParam("aljsId"))
        {
            var formElem = component.find("formElem");
            $A.util.addClass(formElem, "slds-has-error");
            component.set("v.errorMsg",event.getParam("errorMsg"));
        } 
	}
})