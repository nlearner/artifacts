({
	initializeDatePicker : function(component, helper) {
		 
        var j$ = jQuery.noConflict();
        moment.locale($A.get("$Locale.dateFormat"));
        var formatL = moment.localeData().longDateFormat('L'); 
        //Assigning SLDS Static Resource Path To A Variable To Use It With ALJS Initialization
        var assetsLoc = '/resource/slds_214';
        console.log('assetsLoc======',assetsLoc);
        //ALJS Initialization   
        component.set("v.localeDateFormat", formatL); 
        console.log('==$A.get("$Locale.dateFormat")=',$A.get("$Locale.dateFormat"),'===formatL===',formatL);
        j$.aljsInit({
            assetsLocation: assetsLoc, //SLDS Static Resource Path
            scoped: true
        });
 
        //Initializing Datepicker with options To The SLDS Input on document ready.
        console.log('v.dateVal=========',component.get('v.dateVal'));
        console.log('v.dateId=========',component.get('v.dateId'));
        var dateMoment = null;
        if(!$A.util.isEmpty(component.get('v.dateVal')) && component.get('v.dateVal') != 'null')
        {
            dateMoment = moment(component.get('v.dateVal'), 'YYYY-MM-DD').format(component.get("v.localeDateFormat"));
            //dateMoment = moment(component.get('v.dateVal'), "MM/DD/YYYY");
            console.log('moment==========',dateMoment);
        }
        
        j$('#date__'+component.get("v.dateId")).datepicker({
            initDate: dateMoment, //Today Date
            format: component.get("v.localeDateFormat"), //Date Format Of Datepicker Input Field
            onChange: function(datepicker) {
                component.set("v.isValidInput", true);
                var formElem = component.find("formElem");
                $A.util.removeClass(formElem, "slds-has-error");
                component.set("v.errorMsg","");
                
                console.log('datepicker====',datepicker);
                /*remove scrollbar from modal when calendar is closed*/
                j$(".cCard").removeClass("slds-scrollable");
                /*remove scrollbar from modal when calendar is closed*/
                console.log('datformate=1====',datepicker.selectedFullDate._d);
                console.log('datformate=2====',moment(datepicker.selectedFullDate._d));
                console.log('datformate==3===',moment(datepicker.selectedFullDate._d).format('YYYY-MM-DD'));
                var currentSelection = j$('#date__'+component.get("v.dateId")).datepicker('getDate');
                console.log('=currentSelection==',currentSelection);
                var dt = null;
                if(!$A.util.isEmpty(datepicker.selectedFullDate._d))
                {
                    dt = moment(datepicker.selectedFullDate._d).format('YYYY-MM-DD');
                }
                component.set('v.dateVal', dt);
                
            }
        });
        /*add scrollbar to modal when calendar is opened*/
        j$(function()
           {
               j$(".dateField").click(function()
               {
               		j$(".cCard").addClass("slds-scrollable");
               });
               j$(".dateSVG").click(function()
               {
                   j$(".cCard").addClass("slds-scrollable");
               });
           }); 
        /*add scrollbar to modal when calendar is opened*/
	},

    validateInput : function(component)
    {
        var j$ = jQuery.noConflict();
        var elemId = '#date__'+component.get("v.dateId");
        var currentSelection = j$(elemId).datepicker('getDate');
        var actualVal = j$(elemId).val();
        var formElem = component.find("formElem");
        console.log( "Handler for .blur() called.===",currentSelection,'==actual==',actualVal );
        console.log('====currentSelection===='+currentSelection);
        console.log('==typeof==currentSelection===='+typeof currentSelection);
        var dateVal = '';
        if(currentSelection != null && currentSelection != undefined && currentSelection != '')
        {
            console.log('====IN====');
            dateVal = moment(currentSelection).format(component.get("v.localeDateFormat"));
        }
        console.log('====dateVal===='+dateVal);
        if(actualVal != '' && actualVal != dateVal)
        {
            component.set("v.isValidInput", false);
            $A.util.addClass(formElem, "slds-has-error");
            component.set("v.errorMsg","Please select value date picker");
        }
        else
        {
            component.set("v.isValidInput", true);
            $A.util.removeClass(formElem, "slds-has-error");
            component.set("v.errorMsg","");
        }
    }
})