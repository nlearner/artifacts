({
	saveCloneRecord : function(component) {
		if(this.validateInputs(component))
        {
            this.showSpinner(component);
            var action = component.get("c.createClone");
			var campsiteReservn = component.get("v.campsiteReservn");
            console.log('=courseOff=before=',campsiteReservn);
            
            action.setParams({
                "jsonCR" : JSON.stringify(component.get("v.campsiteReservn")), 
            });
			
            action.setCallback(this, function(response) {
                 //store state of response
                 var state = response.getState();
                    
                 if (state === "SUCCESS") { 
                     console.log('==Success==');
                    //set response value in courseoffering attribute on component.
                    console.log("==Success===",response.getReturnValue());
                   	
                     component.set("v.successMsg","Clone created successfully");
                     // close the Modal 
                    window.setTimeout(
                        $A.getCallback(function() { 
                            if (component.isValid()) {
                                
                                 var navEvt = $A.get("e.force:navigateToSObject");
                                 if(navEvt != undefined)
                                 {
                                     navEvt.setParams({
                                     "recordId": response.getReturnValue(),
                                     "slideDevName": "related"
                                     });
                                     navEvt.fire(); 
                                 } 
                            }
                        }), 1000
                    );
                }
              else 
              {
                  console.log("Failed with state: ", state);
                  console.log("Failed response: ", response.getError());
                  console.log("Failed response: ", response.getError()[0].message);
                  component.set("v.errorMsgs", response.getError()[0].message);
              }
                this.hideSpinner(component);
          });
           $A.enqueueAction(action);
        }
        else
        {
            this.hideSpinner(component);
        }
	},
    validateInputs : function(component) {
        var isValid = true;
        var cr = component.get("v.campsiteReservn");
		if(!component.get("v.isStartDateValid"))
        {
            isValid = false;
        }
        if(isValid && !component.get("v.isEndDateValid"))
        {
            isValid = false; 
        }
        if(isValid && component.get("v.isStartDateValid") && $A.util.isEmpty(cr.objCR.Start_Date__c))
        {
            isValid = false;
            this.fireErrorEvent("startDt", "You must enter a value");
        }
        if(isValid && component.get("v.isEndDateValid") && $A.util.isEmpty(cr.objCR.End_Date__c))
        {
            isValid = false;
            this.fireErrorEvent("endDt", "You must enter a value");
        }
        if(isValid && $A.util.isEmpty(cr.campsiteLkp.id))
        {
            isValid = false;
            if(!$A.util.isEmpty(cr.campsiteLkp.label))
            {
                this.fireErrorEvent("campsiteLkp", "Please select value from lookup menu");
            }
            else
            {
                this.fireErrorEvent("campsiteLkp", "You must enter a value");
            }
        } 

        if(isValid && !$A.util.isEmpty(cr.userLkp.label) && $A.util.isEmpty(cr.userLkp.id))
        {
            isValid = false;
            this.fireErrorEvent("userLkp", "Please select value from lookup menu");
        }
        
		return(isValid);
    },
    
    fetchCampsiteReservn : function(component)
    {
        this.showSpinner(component);
    	var action = component.get("c.getCopyCampSiteReservn");
        var crId = component.get("v.recordId");
        console.log('==crId=',crId);
        
        action.setParams({
            "campReservnId" : crId
        });
        console.log('==action=',action);
        console.log('==action=',action.getState());
        action.setCallback(this, function(response) {
        	console.log('==setCallback=',response);
            
             //store state of response
             var state = response.getState();
                
             if (state === "SUCCESS") { 
                 console.log('==Success==',response.getReturnValue());
                 var respParsed = JSON.parse(response.getReturnValue());
                 console.log('==Success==', respParsed);
                 component.set("v.crIconDetails", respParsed.iconDetails);
                 component.set("v.campsiteReservn",respParsed.crWrap);
                 component.set("v.onLoad", true);  
         	}
          else 
          {
              component.set("v.errorMsgs", response.getError()[0].message);
          }
          this.hideSpinner(component);
      });
        $A.enqueueAction(action); 
	},
    
    fireErrorEvent : function(aljsId, errorMsg)
    {
    	var errorEvt = $A.get("e.c:ALJSErrorEvent");
        errorEvt.setParams({
            "errorMsg" : errorMsg,
            "aljsId" : aljsId
    	});     
    	errorEvt.fire(); 
	},
    showSpinner : function(component) {
         console.log('=showSpinner=');
        var spinner = component.find('spinner');
		$A.util.removeClass(spinner, "slds-hide");
    },
    hideSpinner : function(component) {
        console.log('=hideSpinner=');
        var spinner = component.find('spinner');
		$A.util.addClass(spinner, "slds-hide"); 
    },
})