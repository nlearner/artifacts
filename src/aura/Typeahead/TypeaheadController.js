({
    doInit : function(component, event, helper) {
        
        helper.fetchIconDetails(component);    
        
        if(!$A.util.isEmpty(component.get("v.selectedId")))
        {            
            var selectedOption = component.find('SelectedOption');
            $A.util.removeClass(selectedOption, 'slds-hide');
            
            var allOptions = component.find('availableOptions');
            $A.util.addClass(allOptions, 'slds-hide');
            
            var search = component.find('InputSearch');
            $A.util.addClass(search, 'slds-hide');
        }
    },
    
    onChangeSearchText : function(component, event, helper){
        
        var clearIcon = component.find('clearSearchTextIcon');
        $A.util.removeClass(clearIcon, 'slds-hide');
        
        var lookUpIcon = component.find('SearchTextIcon');
        $A.util.addClass(lookUpIcon, 'slds-hide');
        
        if(!$A.util.isEmpty(component.get("v.selectedLabel")))
        	helper.onChangeSearchTextHelper(component, event);
    },
    
    onFocusSearchText : function(component, event, helper){
        var elem = component.find("formElem");
        $A.util.removeClass(elem, 'slds-has-error');
        component.set("v.errorMsg", "");
        console.log('=onFocusSearchText==');
        var clearIcon = component.find('clearSearchTextIcon');
        $A.util.removeClass(clearIcon, 'slds-hide');
        
        var lookUpIcon = component.find('SearchTextIcon');
        $A.util.addClass(lookUpIcon, 'slds-hide');
        if($A.util.isEmpty(component.get('v.selectedLabel')))  
        	helper.onChangeSearchTextHelper(component, event);
    },
    
    onCloseSelectedRecord : function(component, event, helper){
        var elem = component.find("formElem");
        $A.util.removeClass(elem, 'slds-has-error');
        component.set("v.errorMsg", "");
        
        console.log('=onCloseSelectedRecord==');
        component.set('v.selectedId', '');
        component.set('v.selectedLabel', '');
        
        var selectedOption = component.find('SelectedOption');
        $A.util.addClass(selectedOption, 'slds-hide');
        
        var search = component.find('InputSearch');
        $A.util.removeClass(search, 'slds-hide');
        
        var clearIcon = component.find('clearSearchTextIcon');
        $A.util.addClass(clearIcon, 'slds-hide');
        
        var lookUpIcon = component.find('SearchTextIcon');
        $A.util.removeClass(lookUpIcon, 'slds-hide');
        
        var allOptions = component.find('availableOptions');
        $A.util.addClass(allOptions, 'slds-hide');
    },
    
    onSelectRecord : function(component, event, helper){
        console.log('=onSelectRecord==');
        var selectedOption = component.find('SelectedOption');
        $A.util.removeClass(selectedOption, 'slds-hide');
        
        var allOptions = component.find('availableOptions');
        $A.util.addClass(allOptions, 'slds-hide');
        
        var search = component.find('InputSearch');
        $A.util.addClass(search, 'slds-hide');
        
        var selectedIndx = event.currentTarget.dataset.index;
        var listAll = component.get('v.searchResult');
        
        var searchTextIcon = component.find('SearchTextIcon');
        var closeIcon = component.find('clearSearchTextIcon');
        $A.util.addClass(search, 'slds-hide');
        
        component.set('v.selectedId', listAll[selectedIndx].id);
        component.set('v.selectedLabel', listAll[selectedIndx].label);
        var elem = component.find("formElem");
        $A.util.removeClass(elem, 'slds-has-error');
        component.set("v.errorMsg", "");
        
        
    }, 
    
    clearSearchText : function(component, event, helper){
        var elem = component.find("formElem");
        $A.util.removeClass(elem, 'slds-has-error');
        component.set("v.errorMsg", "");
        
        console.log('=clearSearchText==');
        var allOptions = component.find('availableOptions');
        $A.util.addClass(allOptions, 'slds-hide'); 
        var clearIcon = component.find('clearSearchTextIcon');
        $A.util.addClass(clearIcon, 'slds-hide');
        
        var lookUpIcon = component.find('SearchTextIcon');
        $A.util.removeClass(lookUpIcon, 'slds-hide');
        component.set('v.selectedId', '');
        component.set('v.selectedLabel', '');
        /*component.set('v.selectedLabel', '');
        if(!$A.util.isEmpty(component.get("v.selectedLabel")))
        	helper.onChangeSearchTextHelper(component, event);*/
    },
    
    onCloseSelectedRecordFromPill : function(component, event, helper){
       var elem = component.find("formElem");
        $A.util.removeClass(elem, 'slds-has-error');
        component.set("v.errorMsg", "");
        
        console.log('=onCloseSelectedRecordFromPill==');
        var selectedRec = component.get('v.selectedRecords');
        var selectedIndx = event.currentTarget.dataset.index;
        var selectedRec = component.get('v.selectedRecords');
        selectedRec.splice(selectedIndx, 1);
        component.set('v.selectedRecords',selectedRec);
    },
    
    handlerError : function(component, event, helper){
        if(event.getParam("aljsId") == component.get("v.lookupId"))
        {
            var elem = component.find("formElem");
            $A.util.addClass(elem, 'slds-has-error'); 
            component.set("v.errorMsg", event.getParam("errorMsg"));
        }
    }
})