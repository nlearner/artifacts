public class SLDS_FormController {
    
    public Date strDate1               {get;set;}
    public Date strDate2               {get;set;}
    public Date strDate3               {get;set;}
    public Date strDate4               {get;set;}
    
    public String strAmntType1          {get;set;}
    public String strAmntType2          {get;set;}
    public String strAmntType3          {get;set;}
    public String strAmntType4          {get;set;}
    
    public contact cont {get;set;}
    
    public SLDS_FormController(ApexPages.StandardController stdController) { 
        this.cont = (Contact)stdController.getRecord();

    }
   
    public PageReference CreateContctClone(){
       
       system.debug('**** date1'+strDate1);
       system.debug('**** date1'+strDate2);
       system.debug('**** date1'+strDate3);
       system.debug('**** date1'+strDate4);
       system.debug('**** date1'+strAmntType1);
       system.debug('**** date1'+strAmntType2);
       system.debug('**** date1'+strAmntType3);
       system.debug('**** date1'+strAmntType4);
       
       return null;
    }
    
    public pagereference closeModal(){
        return new pagereference('/'+cont.id);
    }
    
}