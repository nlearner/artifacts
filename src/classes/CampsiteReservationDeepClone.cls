public with sharing class CampsiteReservationDeepClone 
{
  //--------------------------------------------------------------------------
  
    @AuraEnabled
    public static String getCopyCampSiteReservn(String campReservnId)
    {
        // Check to make sure all fields are accessible to this user
        Set<String> fieldsToCheck = new Set<String> {
            'id', 'name', 'campsite__c', 'end_date__c', 'start_date__c', 
                                  'user__c'  
        };
        
        LightningUtility.fieldAccessibleCheck(fieldsToCheck, 'Campsite_Reservation__c', true, true, false);
        CampsiteReservnWrap objWrap = new CampsiteReservnWrap();
        if(String.isNotBlank(campReservnId))
        {
            List<Campsite_Reservation__c> lstCR = [	SELECT Id, Name, Campsite__c, End_Date__c, Start_Date__c,
                                                                User__c, User__r.Name, Campsite__r.Name
                                                        	FROM Campsite_Reservation__c 
                                                        	WHERE Id =: campReservnId];
            if(!lstCR.isEmpty())
            {
                system.debug('==lstCR[0]='+lstCR[0]);
                Campsite_Reservation__c objCR = lstCR[0].clone(false);
                system.debug('==objCR='+objCR);
                /* If we have null values for a field, we cannot get corresponding field as an object's property in Lightning. 
                 * That is why binding becomes difficult at lightning side.
                	Even if we use wrapper class instance and return it, same issue exists on Lightning for null values. 
                Hence, using wrapper class + JSON.serializePretty(); with suppressApexObjectNulls = false will retain the field as properties for null values.
                Parsing this json string at the Lightning component side gives proper result.
                */
                /** Set null values explicitly for the non-reference(lookup/MD) fields - START**/
                if(objCR.Start_Date__c == null)
        			objCR.Start_Date__c = null;
        		if(objCR.End_Date__c == null)
        			objCR.End_Date__c = null;
                /** Set null values explicitly for the non-reference(lookup/MD) fields - END**/
                objWrap.objCR = objCR;
                /** (For the lookup fields using another wrapper here)
					For the null lkp/MD field-values (eg: user is lookup field, so it can be null), explicilty setting null for the respective fields.
				**/
				objWrap.campsiteLkp = new SobjectLookupController.ALJSWrapper(objCR.Campsite__r.Name, objCR.Campsite__c);
				objWrap.userLkp = new SobjectLookupController.ALJSWrapper(objCR.User__r.Name, objCR.User__c);                
            }
        }
        CampsiteReservnWrapWithIcon objIconWithCR = new CampsiteReservnWrapWithIcon(objWrap, 'Campsite_Reservation__c');
        return JSON.serializePretty(objIconWithCR, false);  
    }

    @AuraEnabled
    public static String createClone(String jsonCR) 
    {
        String errorMsg = '';
        system.debug('=====jsonCR===='+jsonCR);
        CampsiteReservnWrap objWrap = (CampsiteReservnWrap)JSON.deserialize(jsonCR, CampsiteReservnWrap.class);
        system.debug('======objWrap'+objWrap);
        
        Campsite_Reservation__c objCR = new Campsite_Reservation__c();
        objCR = objWrap.objCR; 
        objCR.User__c = String.isBlank(objWrap.userLkp.id) ? null : Id.valueOf(objWrap.userLkp.id);
        objCR.Campsite__c = String.isBlank(objWrap.campsiteLkp.id) ? null : Id.valueOf(objWrap.campsiteLkp.id);
        system.debug('=objCR==='+objCR);

        try 
        {
            // Check to make sure all fields are accessible to this user
            Set<String> fieldsToCheck = new Set<String> {
                'id', 'name', 'campsite__c', 'end_date__c', 'start_date__c', 
                                      'user__c'  
            };
            LightningUtility.fieldAccessibleCheck(fieldsToCheck, 'Campsite_Reservation__c', false, true, false);
            insert objCR;
        } 
        catch(Exception ex) 
        {
            system.debug('====ex===='+ex.getMessage());
            system.debug('====ex===='+ex.getLineNumber());
            
            throw new AuraHandledException(ex.getMessage()); 
        }
        return objCR.Id;
    }
    
    public class CampsiteReservnWrap
    {
         @AuraEnabled 
         public SobjectLookupController.ALJSWrapper userLkp {get;set;}
         @AuraEnabled 
         public SobjectLookupController.ALJSWrapper campsiteLkp {get;set;}
         @AuraEnabled
         public Campsite_Reservation__c objCR {get;set;}
         /*public CampsiteReservnWrap(Campsite_Reservation__c objCR, SobjectLookupController.ALJSWrapper userLkp, SobjectLookupController.ALJSWrapper campsiteLkp)
         {
             this.objCR = objCR;
             this.userLkp = userLkp;
             this.campsiteLkp = campsiteLkp;
         }*/
        public CampsiteReservnWrap()
        {
            
        } 
    }
    
    public class CampsiteReservnWrapWithIcon
    {
        @AuraEnabled 
        public CampsiteReservnWrap crWrap {get;set;}
        @AuraEnabled
        public LightningUtility.TabIconDetails iconDetails {get;set;}
        public CampsiteReservnWrapWithIcon(CampsiteReservnWrap crWrap, String objName)
        {
            this.crWrap = crWrap;
            this.iconDetails = LightningUtility.fetchTabIconDetails(objName); 
        }
    }
}