public class LightningUtility 
{
    public static void fieldAccessibleCheck(Set<String> fieldsToCheck, String objName, Boolean isReadCheck, Boolean isCreateCheck, Boolean isWriteCheck)
    {
        Map<String,Schema.SObjectField> fieldDescribeTokens = Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();
        String strFldName;
        for(String fldNameExist : fieldDescribeTokens.keySet()) 
        {
            strFldName = fieldDescribeTokens.get(fldNameExist).getDescribe().getName().toLowerCase();
            if(fieldsToCheck.contains(strFldName))
            {
                 system.debug('=='+strFldName+'===strFldName=='+fieldDescribeTokens.get(strFldName)); 
                if( isReadCheck && !fieldDescribeTokens.get(strFldName).getDescribe().isAccessible() || 
                    (strFldName != 'id' && strFldName != 'name' &&
                    (   (isWriteCheck && ! fieldDescribeTokens.get(strFldName).getDescribe().isUpdateable()) 
                        || 
                        (isCreateCheck && ! fieldDescribeTokens.get(strFldName).getDescribe().isCreateable())
                    )
                    )
                    )
                {
                    system.debug('=='+objName+'===fieldAccessibleCheck2=='+strFldName);
                    throw new AuraHandledException(objName + '.' + strFldName + ' is not accessible/writable');  
                }
            }
            
        }
    }
    
    public static TabIconDetails fetchTabIconDetails(String objName)
    {
        TabIconDetails objIconDetail;
        String iconName, colorCode;
        /*if(objName.contains('__c')){*/
                for(Schema.DescribeTabSetResult tsr : Schema.describeTabs()) {
                        for(Schema.DescribeTabResult tr : tsr.getTabs()) {
                        system.debug('%%%%%%'+tr.getSobjectName());
                        

                            if(tr.getSobjectName() == objName){
                                for(Schema.DescribeIconResult icon : tr.getIcons()){
                                    System.debug('getUrl: ' + icon.getUrl());
                                    if(icon.getUrl().contains('.svg')){
                                        List<string> iconUrlStrList = icon.getUrl().split('.svg')[0].split('/');
                                        system.debug('%%%'+iconUrlStrList[iconUrlStrList.size()-1]);
                                        System.debug('getColors: ' + tr.getColors());
                                        for(Schema.DescribeColorResult clr: tr.getColors())
                                        {
                                            if(clr.getTheme() == 'theme4')
                                            {
                                                colorCode = clr.getColor();
                                            }
                                        }
                                        iconName = iconUrlStrList[iconUrlStrList.size()-1];
                                        objIconDetail =  new TabIconDetails(iconName.toLowerCase(), objName.contains('__c') ? 'custom':'standard', colorCode); 
                                        return objIconDetail;
                                    }
                                }
                            }
                        }
                }
            /*}else{
                iconName = objName.toLowerCase();
            }*/
        objIconDetail =  new TabIconDetails(objName.toLowerCase(), objName.contains('__c') ? 'custom':'standard', 'a094ed');
        return objIconDetail;
    }
    
    public class TabIconDetails
    {
        @AuraEnabled
        public String iconName {get;set;}
        @AuraEnabled
        public String colorCode {get;set;} 
        @AuraEnabled
        public String typeOfObj {get;set;}
        public TabIconDetails(String iconName, String typeOfObj, String colorCode)
        {
            this.typeOfObj = typeOfObj;
            this.iconName = iconName;
            this.colorCode = colorCode;
        }
    }
    
    public class SearchInputsWrapper
    { 
        @AuraEnabled
        public String strSelectedField {get;set;}
        @AuraEnabled
        public String strMatchingOperator {get;set;}
        @AuraEnabled
        public String strMatchingVal {get;set;}
        @AuraEnabled
        public String strMatchingLabel {get;set;}
        @AuraEnabled
        public Date dtMatchingDateVal {get;set;}
        @AuraEnabled
        public String dttMatchingDatetimeVal {get;set;}
        @AuraEnabled
        public String strDataType {get;set;}
        @AuraEnabled
        public Boolean isValidInput {get;set;} 
        public SearchInputsWrapper()
        {
            
        }
    }
}