public class SLDS_ContactController {

    public class multiSelectJSONWrap{

    	public String id;
    	public String label;
	
    	public multiSelectJSONWrap(String id, String label) {
    	    
    	    this.id = id;
    	    this.label = label;
    	}
    }
    
    public String strSelectedMultiselect   {get;set;}
    public String strUnselectedMultiselect {get;set;}
    public String strSelectedMultiselect1   {get;set;}
    public String strUnselectedMultiselect1 {get;set;}
    public Contact objContact {get;set;} 
    private Id ContactId;
     
    public SLDS_ContactController(ApexPages.StandardController controller) {
        
        ContactId = ((Contact) controller.getRecord()).id;
        slds_contactDetails();
    }
    
    public void slds_contactDetails(){
        
        List<multiSelectJSONWrap> lstJSONPicklist = new List<multiSelectJSONWrap>();     
        List<multiSelectJSONWrap> lstJSONPicklistSelected = new List<multiSelectJSONWrap>();
        objContact = [SELECT Id, Multi_select__c,Multi_Select2__c FROM Contact WHERE Id=: ContactId];
        Set<String> setSelectedValues = new Set<String>();
        if(String.isNotBlank(objContact.Multi_select__c)){
            
            setSelectedValues.addAll(objContact.Multi_select__c.split(';'));
        }
        if(String.isNotBlank(objContact.Multi_select2__c)){
            
            setSelectedValues.addAll(objContact.Multi_select2__c.split(';'));
        }
        
        system.debug('objContact.Multi_select__c=========='+objContact.Multi_select__c);
        
        Schema.DescribeFieldResult fieldResult = Contact.Multi_select__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
             if(setSelectedValues.contains(f.getValue())){
                 
                 lstJSONPicklistSelected.add(new multiSelectJSONWrap(f.getValue(), f.getLabel()));
             }else{
                 
                 lstJSONPicklist.add(new multiSelectJSONWrap(f.getValue(), f.getLabel()));
             }
            
        }
        
        strSelectedMultiselect = JSON.serialize(lstJSONPicklistSelected);
        strUnselectedMultiselect = JSON.serialize(lstJSONPicklist);
        
        /*multiselect 2*/
        lstJSONPicklist = new List<multiSelectJSONWrap>();     
        lstJSONPicklistSelected = new List<multiSelectJSONWrap>();
        fieldResult = Contact.Multi_select2__c.getDescribe();
        ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
             if(setSelectedValues.contains(f.getValue())){
                 
                 lstJSONPicklistSelected.add(new multiSelectJSONWrap(f.getValue(), f.getLabel()));
             }else{
                 
                 lstJSONPicklist.add(new multiSelectJSONWrap(f.getValue(), f.getLabel()));
             }
            
        }
        
        strSelectedMultiselect1 = JSON.serialize(lstJSONPicklistSelected);
        strUnselectedMultiselect1 = JSON.serialize(lstJSONPicklist);
    }
    
    public void saveMultiselect(){
        
        List<multiSelectJSONWrap> lstJSONPicklist = new List<multiSelectJSONWrap>();     
        system.debug('strSelectedMultiselect============='+strSelectedMultiselect);
        lstJSONPicklist = (List<multiSelectJSONWrap>) JSON.deserialize(strSelectedMultiselect, List<multiSelectJSONWrap>.class);
        system.debug('lstJSONPicklist==========='+lstJSONPicklist);
        
        String selectedValues = '';
        for(multiSelectJSONWrap objJson : lstJSONPicklist){
            
            selectedValues += objJson.id +';';
        }
        
        objContact.Multi_select__c = selectedValues;
        update objContact;
        
        slds_contactDetails();
        
    }
}