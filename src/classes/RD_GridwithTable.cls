public class RD_GridwithTable
{
    public Lookups__c objLookup {get;set;}
    public List<Lookups__c> lstLookup {get;set;}
    public Boolean isEnterExtraDetails {get;set;}
    public RD_GridwithTable(ApexPages.standardController sc)
    {
        objLookup = new Lookups__c();
        isEnterExtraDetails = false;
        fetchRecords();
    }
    
    private void fetchRecords()
    {
        lstLookup = new List<Lookups__c>([SELECT Picklist2__c, Id,Name,Account__c,Chkbx_formula__c, 
            Opportunity__c,User__c,SLASerialNumber__c,Picklist__c,Multiselect__c,Contact__c,Contact__r.Name,
                            Account__r.Name,Opportunity__r.Name,User__r.Name,createdDate,
                            Amount__c, Are_you_interested__c, Description__c, Email__c, 
                            Entry_Date__c, Formula__c, Num_of_loc__c, Opt_Name__c,Campaign__c,
                            Percent__c, Phone__c, URL__c, Self_Lookup__c, CLose_Date__C
                            FROM Lookups__c]);
    }
    
    public PageReference insertLookup()
    {
        try
        {
            insert objLookup;
            lstLookup.add(objLookup);
            objLookup = new Lookups__c();
        }
        catch(Exception e)
        {
            system.debug('===e==='+e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        return null;
    }
}