public class ALJS_GenericCompController{
    
    public string object_name{get;set;}
    public string field_name{get;set;}
    public String strSelectedValue {get;set;}
    
    public ALJS_GenericCompController(){
      
    }
    
    public List<SelectOption> getPicklistOptions(){
        
        List<selectOption> options = new List<selectOption>(); 
        List<Schema.PicklistEntry> pick_list_values = Schema.getGlobalDescribe().get(object_name).getDescribe().fields.getMap().get(field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        
        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
                  
            options.add(new selectOption(a.getValue(), a.getLabel())); //add the value and label to our final list
        }
        return options; 
      
    }
    
    public String getAutoCompleteArray(){
        
        List<ALJSWrapperClass> lstArr = new List<ALJSWrapperClass>();
        String query = 'Select Id,Name FROM '+ object_name;
        
        for(sObject objsObject : Database.query(query)) {
            
            lstArr.add(new ALJSWrapperClass(String.valueOf(objsObject.get('Name')), String.valueOf(objsObject.get('Id'))));
        }  
        return JSON.serializePretty(lstArr);
    }
    
    public SelectedUnSelectedWrapper getPopulatedMultiPicklistVals()
    {
        SelectedUnSelectedWrapper objWrap;
        List<ALJSWrapperClass> lstJSONPicklist = new List<ALJSWrapperClass>();     
        List<ALJSWrapperClass> lstJSONPicklistSelected = new List<ALJSWrapperClass>();
        Set<String> setSelectedValues = new Set<String>();
        
        if(String.isNotBlank(strSelectedValue)){
            
            setSelectedValues.addAll(strSelectedValue.split(';'));
        }
        
        List<Schema.PicklistEntry> ple = Schema.getGlobalDescribe().get(object_name).getDescribe().fields.getMap().get(field_name).getDescribe().getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            if(setSelectedValues.contains(f.getValue())){
            
                lstJSONPicklistSelected.add(new ALJSWrapperClass(f.getValue(), f.getLabel()));
            }else{
            
                lstJSONPicklist.add(new ALJSWrapperClass(f.getValue(), f.getLabel()));
            }
            
        }
        
        objWrap = new SelectedUnSelectedWrapper(JSON.serialize(lstJSONPicklistSelected), JSON.serialize(lstJSONPicklist));
        return objWrap;
    }
    
    public class SelectedUnSelectedWrapper
    {
        public String strSelected   {get;set;}
        public String strUnSelected {get;set;}
        
        public SelectedUnSelectedWrapper(String strSelected, String strUnSelected)
        {
            this.strSelected = strSelected;
            this.strUnSelected = strUnSelected;
        }
    }
}