public class ALJS_DateGenericCompController {
    
    public String strLocale { get; set; }
    public String strTzId { get; set; }
    public ALJS_DateGenericCompController() {
        strTzId = UserInfo.getTimeZone().getID();
        strLocale = UserInfo.getLocale();
    }
}