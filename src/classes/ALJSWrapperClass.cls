public class ALJSWrapperClass {
    
        public String label {get;set;}
        public String id {get;set;}
        
        public ALJSWrapperClass(String lb, String strId) {
            
            this.label = lb;
            this.id = strId.replaceAll('[^a-zA-Z0-9-]','');
        }
    }