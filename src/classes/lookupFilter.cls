global with sharing class lookupFilter {
    
    public String field_name { get; set; }
    public String iconName{ get; set; }
    public String strSelectedValue {get;set;}
    public sObject sObjectInstance {get;set;}
    public String inputValue  {get;set;} 
    public lookupFilter () {}
    private static string inputLookupValue;
    public Boolean blnNormalPicklist {get;set;}
    public List<SelectOption>  lstNormalSelectOption {get;set;} 
    public List<SelectOption>  lstMultiUnSelected {get;set;} 
    public List<SelectOption>  lstMultiSelected {get;set;} 
    public String lookupId {get;set;}
    
    
    public String object_name { 
        get{
            if(sObjectInstance != null){
                return sObjectInstance.getSObjectType().getDescribe().getName();    
            }
            return object_name;
        } 
        set;
    }
    
    public String lookupReferenceObjectName{
        get{
          iconName = '';
          
            String objName = getFieldDescribe().getReferenceTo()[0].getDescribe().getName();
            String idVal = String.valueOf(sObjectInstance.get(field_name));
            if(String.isBlank(idVal)) 
                strSelectedValue = '';
            else
            {
                for(sObject objsObject : database.query('SELECT Id,Name FROM '+ objName +' WHERE Id=\''+ idVal +'\'')){
                    lookupId = String.valueOf(objsObject.get('Id')); 
                    strSelectedValue = String.valueOf(objsObject.get('Name'));    
                }
            }
            
            return objName;
        }
        set;
    }
    
    public String lookupReferenceIconName{
        get{
        
          iconName = '';
          
            String objName = getFieldDescribe().getReferenceTo()[0].getDescribe().getName();
            String idVal = String.valueOf(sObjectInstance.get(field_name));
            if(String.isBlank(idVal)) 
                strSelectedValue = '';
            else
            {
                for(sObject objsObject : database.query('SELECT Id,Name FROM '+ objName +' WHERE Id=\''+ idVal +'\'')){
                    lookupId = String.valueOf(objsObject.get('Id')); 
                    strSelectedValue = String.valueOf(objsObject.get('Name'));    
                }
            }
            
            if(objName.contains('__c')){
                for(Schema.DescribeTabSetResult tsr : Schema.describeTabs()) {
                    String appLabel = tsr.getLabel();
                    //if (appLabel == 'Financials') {
                        for(Schema.DescribeTabResult tr : tsr.getTabs()) {
                        system.debug('%%%%%%'+tr.getSobjectName());
                            if(tr.getSobjectName() == objName){
                                for(Schema.DescribeIconResult icon : tr.getIcons()){
                                    System.debug('getUrl: ' + icon.getUrl());
                                    if(icon.getUrl().contains('.svg')){
                                        List<string> iconUrlStrList = icon.getUrl().split('.svg')[0].split('/');
                                        system.debug('%%%'+iconUrlStrList[iconUrlStrList.size()-1]);
                                        iconName = iconUrlStrList[iconUrlStrList.size()-1];
                                    }
                                }
                            }
                        }
                    //}
                }
            }else{
                iconName = objName;
            }
            
            return iconName ;
        }
        set;
    }
   
    public Boolean isReadOnly {
        get {
            return !getFieldDescribe().isUpdateable();
        }
    }

    public Boolean isInput {
        get {
            DisplayType dispType = getFieldDescribe().getType();
            return (dispType == Schema.DisplayType.String ||
                dispType == Schema.DisplayType.Currency ||
                dispType == Schema.DisplayType.Email ||
                dispType == Schema.DisplayType.Double ||
                dispType == Schema.DisplayType.Integer ||
                dispType == Schema.DisplayType.Percent ||
                dispType == Schema.DisplayType.URL ||
                dispType == Schema.DisplayType.Phone) ? true : false;
        }
    }
    
    public Boolean isURL {
        get {
            DisplayType dispType = getFieldDescribe().getType();
            return dispType == Schema.DisplayType.URL ? true : false;
        }
    }
    
    public Boolean isText {
        get {
            DisplayType dispType = getFieldDescribe().getType();
            return (dispType == Schema.DisplayType.Phone || dispType == Schema.DisplayType.String) ? true : false;
        }
    }
    
    public Boolean isEmail {
        get {
            DisplayType dispType = getFieldDescribe().getType();
            return dispType == Schema.DisplayType.Email ? true : false;
        }
    }
    
     public Boolean isNumber {
        get {
            
            DisplayType dispType = getFieldDescribe().getType();
            return 
                dispType == Schema.DisplayType.Double 
                ? true : false;
            
        }
    }
    
    public Boolean isPercent {
        get {
            DisplayType dispType = getFieldDescribe().getType();
            return dispType == Schema.DisplayType.Percent ? true : false;
        }
    }
    
    public Boolean isCurrency {
        get {
            DisplayType dispType = getFieldDescribe().getType();
            return dispType == Schema.DisplayType.Currency ? true : false;
        }
    }
    
    public Boolean isTextarea {
        get {
            DisplayType dispType = getFieldDescribe().getType();
            return dispType == Schema.DisplayType.TextArea ? true : false;
        }
    }

    public Boolean isPicklist {
        get {
            DisplayType dispType = getFieldDescribe().getType();
            return dispType == Schema.DisplayType.Picklist ? true : false;
        }
    }

    public Boolean isDependentPicklist {
        get {
            return getFieldDescribe().isDependentPicklist();
        }
    }

   public Boolean isMultiPicklist {
        get {
            DisplayType dispType = getFieldDescribe().getType();
            return dispType == Schema.DisplayType.MultiPicklist ? true : false;
        }
    }

    public Boolean isCheckbox {
        get {
            DisplayType dispType = getFieldDescribe().getType();
            return dispType == Schema.DisplayType.Boolean ? true : false;
        }
    }

    public Boolean isDatetime {
        get {
            DisplayType dispType = getFieldDescribe().getType();
            return (dispType == Schema.DisplayType.Date ||
                dispType == Schema.DisplayType.Datetime) ? true : false;
        }
    }

    public Boolean isLookup {
        get {
            DisplayType dispType = getFieldDescribe().getType();
            return dispType == Schema.DisplayType.reference ? true : false;
        }
    }

    public SObjectField getSField() {
        if (String.isNotBlank(object_name))
            return Schema.getGlobalDescribe().get(object_name).getDescribe().fields.getMap().get(field_name);
        else
            return null;
    }

    public DescribeFieldResult getFieldDescribe() { 
        
        SObjectField sf = getSField();
        system.debug('=getFieldDescribe=sf===='+sf);
        return sf != null ? sf.getDescribe() : null;
    }

    public String getPicklistOptions(){
        List<ALJSWrapperClass> lstJSONPicklist = new List<ALJSWrapperClass>();
        lstJSONPicklist.add(new ALJSWrapperClass('None','None'));
        if(blnNormalPicklist!= null && blnNormalPicklist)
        {
            if(lstNormalSelectOption != null && lstNormalSelectOption.isEmpty())
            for (SelectOption a : lstNormalSelectOption) { //for all values in the picklist list
                lstJSONPicklist.add(new ALJSWrapperClass(a.getLabel(), a.getValue()));
            }
        }
        else
        {
            List<Schema.PicklistEntry> pick_list_values = Schema.getGlobalDescribe().get(object_name).getDescribe().fields.getMap().get(field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        
            for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
                lstJSONPicklist.add(new ALJSWrapperClass(a.getLabel(), a.getValue()));
            }
        }
        String  strVal = JSON.serialize(lstJSONPicklist);
        return strVal; 
    }
    
    @RemoteAction
    global static String getAutoCompleteArray(String objName, String str, String parentFldName) {
        List<ALJSWrapperClass> lstArr = new List<ALJSWrapperClass>();
        String query;
        
        if(str == null) {
            query = 'Select Id,Name FROM '+ objName + ' ' + parentFldName + ' =: str ORDER BY LastViewedDate DESC LIMIT 500';
        }else {
            query = 'Select Id,Name FROM '+ objName + ' WHERE ' + parentFldName + ' =: str ORDER BY LastViewedDate DESC LIMIT 500';
        } 
        for(sObject objsObject : Database.query(query)) {
            
            lstArr.add(new ALJSWrapperClass(String.valueOf(objsObject.get('Name')), String.valueOf(objsObject.get('Id'))));
        }  
        return JSON.serializePretty(lstArr);
    }
    
    //get the record with the search-term on object
    @RemoteAction
    global static String getSearchedArray(String searchTerm, string objName,string parentId, String parentFldName) {
      
        List<ALJSWrapperClass> lstArr = new List<ALJSWrapperClass>();
        String query;
        if(parentId != null)
            query = 'Select Id,Name FROM '+ objName + ' where ' + parentFldName + ' =: parentId AND Name like \'%' + searchTerm + '%\' limit 50';
        
        for(sObject objsObject : Database.query(query)) {
            lstArr.add(new ALJSWrapperClass(String.valueOf(objsObject.get('Name')), String.valueOf(objsObject.get('Id'))));
        }  
        return JSON.serialize(lstArr);
    }
    
    public SelectedUnSelectedWrapper getPopulatedMultiPicklistVals()
    {
        List<ALJSWrapperClass> lstJSONPicklist = new List<ALJSWrapperClass>();     
        List<ALJSWrapperClass> lstJSONPicklistSelected = new List<ALJSWrapperClass>();
        Set<String> setSelectedValues = new Set<String>();
        
        if(lstMultiUnSelected != NULL)
        {
            for(SelectOption objPLE : lstMultiUnSelected)
            {
                lstJSONPicklist.add(new ALJSWrapperClass(objPLE.getValue(), objPLE.getValue()));
            }
            for(SelectOption objPLE : lstMultiSelected)
            {
                lstJSONPicklistSelected.add(new ALJSWrapperClass(objPLE.getValue(), objPLE.getValue()));
            }
        }
        else
        {
            List<Schema.PicklistEntry> ple = Schema.getGlobalDescribe().get(object_name).getDescribe().fields.getMap().get(field_name).getDescribe().getPicklistValues();
        
            if(String.isNotBlank(strSelectedValue))
                setSelectedValues.addAll(strSelectedValue.split(';'));
            
            for(Schema.PicklistEntry f : ple)
            {
                if(setSelectedValues.contains(f.getValue()))
                    lstJSONPicklistSelected.add(new ALJSWrapperClass(f.getValue(), f.getLabel()));
                else
                    lstJSONPicklist.add(new ALJSWrapperClass(f.getValue(), f.getLabel()));
            }
        
        }
       
        SelectedUnSelectedWrapper objWrap = new SelectedUnSelectedWrapper(JSON.serialize(lstJSONPicklistSelected), JSON.serialize(lstJSONPicklist));
        return objWrap;
    }
    
    public static String extractSelectedVals(String strSelectedMultiselectVal)
    {
        List<ALJSWrapperClass> lstJSONPicklist = new List<ALJSWrapperClass>();     
        lstJSONPicklist = (List<ALJSWrapperClass>) JSON.deserialize(strSelectedMultiselectVal, List<ALJSWrapperClass>.class);
        String selectedValues = '';
        
        for(ALJSWrapperClass objJson : lstJSONPicklist){
            selectedValues += objJson.label +';';
        }
        return selectedValues;
    }
    
    public class SelectedUnSelectedWrapper
    {
        public String strSelected   {get;set;}
        public String strUnSelected {get;set;}
        
        public SelectedUnSelectedWrapper(String strSelected, String strUnSelected)
        {
            this.strSelected = strSelected;
            this.strUnSelected = strUnSelected;
        }
    }
}