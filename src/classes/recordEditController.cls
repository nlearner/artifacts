/* Class Name               : recordEditController
   Created By & On          : Nayana SLDS & 12/2/16
   Last Modified By By & On : Nayana SLDS & 12/2/16 */
   
public class recordEditController {
    
    /*Start - Global Variables*/
    public Lookups__c objLookup                                     { get; set; }
    public lookupFieldValuesdWrapper objlookupFieldValuesdWrapper   { get; set; }
    /*End - Global Variables*/
    
    /*Start - Constructor*/
    public recordEditController(ApexPages.StandardController controller) {
        
        //Method to initialize variables
        init();
        
        if(controller.getId() != null) {
            
            objLookup = [SELECT Id,
                                Account__c,
                                Are_you_interested__c,
                                Entry_Date__c,
                                Opt_Name__c
                           FROM Lookups__c
                          WHERE ID=:controller.getId()];
        }
    }
    /*End - Constructor*/
    
    /*Start - saveLookUpData method to save the Data from the Page*/ 
    public void saveLookUpData() {
        
        try {
            objLookup.Account__c = (String.isEmpty(objlookupFieldValuesdWrapper.strAccount)) ? null : objlookupFieldValuesdWrapper.strAccount; 
            objLookup.Are_you_interested__c = objlookupFieldValuesdWrapper.boolAreYouInterestedValue;
            objLookup.Entry_Date__c = (!String.isEmpty(objlookupFieldValuesdWrapper.strEntryDateValue)) ? null : Date.parse(objlookupFieldValuesdWrapper.strEntryDateValue);
            objLookup.Opt_Name__c = (String.isEmpty(objlookupFieldValuesdWrapper.strOptNameValue)) ? null : objlookupFieldValuesdWrapper.strOptNameValue; 
            
            update objLookup;
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Record has been saved!')); 
        }
        
        catch(exception e) {
            
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage())); 
        }
        
        
    }
    /*End - saveLookUpData method to save the Data from the Page*/
    
    /*Start - init to initialize variables*/
    public void init() {
        
        objLookup = new Lookups__c();
        objlookupFieldValuesdWrapper = new lookupFieldValuesdWrapper();
    }
    /*End - init to initialize variables*/
    
    /*Start - Inner Class containg Lookup Field Values*/
    public class lookupFieldValuesdWrapper {
        
        public String strAccount                             { get; set; }
        public String strEntryDateValue                      { get; set; }
        public String strOptNameValue                        { get; set; }
        
        public Boolean boolAreYouInterestedValue             { get; set; }
    }
    /*End - Inner Class containg Lookup Field Values*/
}