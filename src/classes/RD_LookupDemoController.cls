public class RD_LookupDemoController
{
    public Lookups__c objLookup {get;set;}
    public LookupsWrapper objLookupWrap {get;set;}
    public String dateTimeToBind {get;set;}
  public String instName {get;set;}
  public String strFldToBind {get;set;}
  public String strLocale  {get;set;}
  
    public RD_LookupDemoController(ApexPages.standardController sc)
    {
        strLocale = UserInfo.getLocale();
        objLookupWrap = new LookupsWrapper();
        objLookup = new Lookups__c();
        if(sc.getId() != null)
        {
            objLookup = [SELECT Picklist2__c, Id,Name,Account__c,Chkbx_formula__c, 
            Opportunity__c,User__c,SLASerialNumber__c,Picklist__c,Multiselect__c,Contact__c,Contact__r.Name,
                            Account__r.Name,Opportunity__r.Name,User__r.Name,createdDate,
                            Amount__c, Are_you_interested__c, Description__c, Email__c, 
                            Entry_Date__c, Formula__c, Num_of_loc__c, Opt_Name__c,Campaign__c,
                            Percent__c, Phone__c, URL__c, Self_Lookup__c, CLose_Date__C
                            FROM Lookups__c Where Id =: sc.getId() ]; 
            
        }
        
    }
    
    public PageReference saveLookUps(){
        
        try
        {
             system.debug('objLookup============'+objLookupWrap.strUser);
            system.debug('objLookup=====strAccount======='+objLookupWrap.strAccount);
            
            system.debug('strDateVal============'+objLookupWrap.strDateVal);
            system.debug('strMultiVal============'+objLookupWrap.strMultiVal); 
            
            //objLookup.Opportunity__c = String.isBlank(objLookupWrap.strOpp) ? null : objLookupWrap.strOpp ;
            objLookup.Account__c = String.isBlank(objLookupWrap.strAccount) ? null : objLookupWrap.strAccount ;
            if(objLookup.Contact__c == null)
                objLookup.Contact__c = String.isBlank(objLookupWrap.strContact) ? null : objLookupWrap.strContact ;
            //objLookup.User__c = String.isBlank(objLookupWrap.strUser) ? null : objLookupWrap.strUser ;
            //objLookup.Picklist__c = objLookupWrap.strPickVal;
            objLookup.Entry_Date__c = String.isNotBlank(objLookupWrap.strDateVal) ? convertToDate(objLookupWrap.strDateVal) : null;
            
            objLookup.Multiselect__c = GenericInputFieldController.extractSelectedVals(objLookupWrap.strMultiVal);
            
            system.debug('objLookup============'+objLookup);  
            upsert objLookup;
        }
        catch(Exception e)
        {
            system.debug('===e==='+e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
      
        return null;
        
    }
    
    public PageReference saveLoookup2()
    {
        try
        {
            upsert objLookup;
        }
        catch(Exception e)
        {
            system.debug('===e==='+e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        return null;
        
    }
    
    // Added by Nayana
    private Date convertToDate(String strDate)
    {
        Date dtVal;
        if(String.isNotBlank(strDate) && strDate != 'UNDEFINED')
        {
            List<String> lstDateParts = strDate.split('/');
            dtVal = Date.newInstance(Integer.valueOf(lstDateParts[2]), Integer.valueOf(lstDateParts[1]), Integer.valueOf(lstDateParts[0]));
            
        }
        return dtVal;
    }
    
    public class LookupsWrapper
    {
        public String strAccount {get;set;}
        public String strUser {get;set;}
        public String strContact {get;set;}
        public String strOpp {get;set;}
        public String strDateVal    {get;set;}
        public String strPickVal    {get;set;}
        public String strMultiVal    {get;set;}
        public Decimal intSerialNumVal    {get;set;} 
        public Decimal intNumOfLocVal    {get;set;}
        public String strDescVal    {get;set;}
        public Boolean blnInterestedVal    {get;set;}
        public String strUrlVal    {get;set;}
        public Decimal decAmountVal {get;set;}
        public String strPhnVal {get;set;}
        public Decimal decPercentval  {get;set;}
        public String strOptNameVal {get;set;}
        public String strEmailVal {get;set;}
    }
    
    public void bindDateTimeField()
    {
      system.debug('instName======'+instName); 
      system.debug('dateTimeToBind======'+dateTimeToBind);
      system.debug('strFldToBind======'+strFldToBind);
      List<String> lstDtSplit = new List<String>();
      DateTime dt;
      if(String.isNotBlank(dateTimeToBind))
        lstDtSplit = dateTimeToBind.split(' ');
      system.debug('===lstDtSplit==='+lstDtSplit);
      if(!lstDtSplit.isEmpty())
      {
          Date d = Date.parse(lstDtSplit[0]);
          List<String> lstTimeSplit = lstDtSplit[1].split(':');
          Time t = Time.newInstance(Integer.valueOf(lstTimeSplit[0]),Integer.valueOf(lstTimeSplit[1]),0,0);
            system.debug('===Date d===='+d);
            system.debug('===Time t===='+t);
            dt = DateTime.newInstance(d,t);
      }
      //DateTime dt = (DateTime)JSON.deserialize(
      //         dateTimeToBind, DateTime.class);
      system.debug('dt======'+dt);
        //if(instName == 'startMeeting' && strFldToBind == 'Date_Time__c')
            //startMeeting.Date_Time__c = dt;
       // else 
            //endMeeting.Date_Time__c = dt;
      
  }
  
  
}