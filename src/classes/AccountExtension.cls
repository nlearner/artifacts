public class AccountExtension {
    
    public List<Account> accounts {get;set;}
    public AccountExtension(ApexPages.StandardSetController controller){
        
        accounts = new List<Account> ([SELECT Id,Name,AccountNumber,AnnualRevenue,CreatedDate,BillingState,
                                        Active__c,Type,SLAExpirationDate__c,MultiselectAcc__c,Some_Date__c,
                                        SLASerialNumber__c,Description,test_Checkbox__c,NumberofLocations__c,Website,ownerId,Owner.Name
                                                            FROM Account LIMIT 5]);
    }

}