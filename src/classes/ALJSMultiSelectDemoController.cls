public class ALJSMultiSelectDemoController {
    
    public Map<String, Schema.SObjectField> fieldMap = Schema.sObjectType.Contact.fields.getMap();
    public String strSelectedMultiselect1 {get;set;}
    public String strUnselectedMultiselect1 {get;set;}
    public String strSelectedMultiselect2   {get;set;}
    public String strUnselectedMultiselect2 {get;set;}
    private Map<String, String> mapSelectedToUnselected = new Map<String, String>();
    public Contact objContact {get;set;} 
    private Id ContactId;
     
    public ALJSMultiSelectDemoController(ApexPages.StandardController controller) {
        
        ContactId = ((Contact) controller.getRecord()).id;
        fetchDetails();
    }
    
    public void fetchDetails(){
        
        SelectedUnSelectedWrapper objWrap;
        
        objContact = [SELECT Id, Multi_select__c, Multi_Select2__c FROM Contact WHERE Id=: ContactId];
        
    }
    
    public void saveMultiselect(){
        
        
        objContact.Multi_select__c = extractSelectedVals(strSelectedMultiselect1);
        objContact.Multi_select2__c = extractSelectedVals(strSelectedMultiselect2);
        
        update objContact;
        
        fetchDetails();
    }
    
    private String extractSelectedVals(String strSelectedMultiselectVal)
    {
        List<ALJSWrapperClass> lstJSONPicklist = new List<ALJSWrapperClass>();     
        system.debug('strSelectedMultiselect============='+strSelectedMultiselectVal);
        lstJSONPicklist = (List<ALJSWrapperClass>) JSON.deserialize(strSelectedMultiselectVal, List<ALJSWrapperClass>.class);
        system.debug('lstJSONPicklist==========='+lstJSONPicklist);
        
        String selectedValues = '';
        for(ALJSWrapperClass objJson : lstJSONPicklist){
            
            selectedValues += objJson.id +';';
        }
        return selectedValues;
    }
    
    public class SelectedUnSelectedWrapper
    {
        public String strSelected;
        public String strUnSelected;
        public SelectedUnSelectedWrapper(String strSelected, String strUnSelected)
        {
            this.strSelected = strSelected;
            this.strUnSelected = strUnSelected;
        }
    }
}