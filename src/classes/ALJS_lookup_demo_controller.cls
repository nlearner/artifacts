public class ALJS_lookup_demo_controller {

    public Lookups__c objLookup {get;set;}
    public Account objAccount {get;set;}
    public String strAccount {get;set;}
    public String strUser {get;set;}
    public String strContact {get;set;}
    public String strOpp {get;set;}
    public String strDateVal    {get;set;}
    public String strPickVal    {get;set;}
    public String strMultiVal    {get;set;}
    public String strSerialNumVal    {get;set;} 
    public Integer intNumOfLocVal    {get;set;}
    public String strTextAreaVal    {get;set;}
    public Boolean blnChkVal    {get;set;}
    public String strUrlVal    {get;set;}
    public String MultiselectAccVal {get;set;}
    public String typePickVal {get;set;}
    public String someDateVal {get;set;} 
    public Decimal annRevVal {get;set;}
    public String accName {get;set;}
    public String activePickVal {get;set;}
    
    public ALJS_lookup_demo_controller(ApexPages.StandardController controller) {

        if(controller.getId() != null)
        {
            objLookup = [SELECT Id,Name,Account__c,Campaign__c,Campaign__r.Name,Self_Lookup__c,Self_Lookup__r.Name,Contact__c,Opportunity__c,User__c,Contact__r.Multi_select__c,SLASerialNumber__c,Picklist__c,Multiselect__c,
                            Account__r.Name,Contact__r.Name,Opportunity__r.Name,User__r.Name,Account__r.SLAExpirationDate__c,Account__r.UpsellOpportunity__c,Account__r.SLASerialNumber__c,createdDate
                            FROM Lookups__c Where Id =: controller.getId() ];
            objAccount = [SELECT Name,Description ,Type, MultiselectAcc__c, AnnualRevenue, Some_Date__c, Active__c, test_Checkbox__c ,SLAExpirationDate__c,UpsellOpportunity__c,SLASerialNumber__c,NumberofLocations__c,Website,test_Rich_text__c  FROM Account WHERE Id='001280000122Arn'];
            
        }
        else
        {
            objLookup = new Lookups__c();
            objAccount = new Account(Name='test84587555');
        }
    }
    
    public PageReference saveLookUps(){
        
        system.debug('objLookup============'+strUser);
        system.debug('objLookup=====strAccount======='+strAccount);
        
        system.debug('strDateVal============'+strDateVal);
        system.debug('strPickVal============'+strPickVal);
        system.debug('strMultiVal============'+strMultiVal);
        system.debug('intSerialNumVal============'+strSerialNumVal);
        system.debug('intNumOfLocVal============'+intNumOfLocVal);
        system.debug('strTextAreaVal============'+strTextAreaVal);
        system.debug('blnChkVal============'+blnChkVal);
        system.debug('strUrlVal============'+strUrlVal);
        system.debug('MultiselectAccVal============'+MultiselectAccVal);
        system.debug('typePickVal============'+typePickVal);
        system.debug('someDateVal============'+someDateVal);
        system.debug('annRevVal============'+annRevVal);
        system.debug('accName============'+accName);
        system.debug('activePickVal============'+activePickVal);
        objLookup.Opportunity__c = String.isBlank(strOpp) ? null : strOpp ;
        objLookup.Account__c = String.isBlank(strAccount) ? null : strAccount ;
        if(objLookup.Contact__c == null)
            objLookup.Contact__c = String.isBlank(strContact) ? null : strContact ;
        objLookup.User__c = String.isBlank(strUser) ? null : strUser ;
        objLookup.Picklist__c = strPickVal;
        objLookup.Multiselect__c = GenericInputFieldController.extractSelectedVals(strMultiVal);
        
        system.debug('objLookup============'+objLookup);  
        upsert objLookup;
        
        
        objAccount.Name = accName;
        objAccount.Description = strTextAreaVal;
        objAccount.test_Checkbox__c = blnChkVal;
        objAccount.SLAExpirationDate__c = String.isNotBlank(strDateVal) ? Date.parse(strDateVal) : null;
        objAccount.SLASerialNumber__c = strSerialNumVal;
        objAccount.NumberofLocations__c = intNumOfLocVal;
        objAccount.Website = strUrlVal;
        objAccount.Type = typePickVal;
        objAccount.MultiselectAcc__c = GenericInputFieldController.extractSelectedVals(MultiselectAccVal);
        objAccount.AnnualRevenue = annRevVal;
        objAccount.Some_Date__c = String.isNotBlank(someDateVal) ? Date.parse(someDateVal) : null;
        objAccount.Active__c = activePickVal;
        //objAccount.test_Rich_text__c = ;
        upsert objAccount;
        return null;
    }
}