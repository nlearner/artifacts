public class LookupsEditController
{

    public Lookups__c objLookup {get;set;}
    public LookupsWrapper objLookupWrap {get;set;}
    public LookupsEditController(ApexPages.StandardController controller) {

        if(controller.getId() != null)
        {
            objLookup = [SELECT Picklist2__c, Id,Name,Account__c,Chkbx_formula__c, Opportunity__c,User__c,SLASerialNumber__c,Picklist__c,Multiselect__c,Contact__c,Contact__r.Name,
                            Account__r.Name,Opportunity__r.Name,User__r.Name,createdDate,
                            Amount__c, Are_you_interested__c, Description__c, Email__c, Entry_Date__c, Formula__c, Num_of_loc__c, Opt_Name__c,
                            Percent__c, Phone__c, URL__c
                            FROM Lookups__c Where Id =: controller.getId() ]; 
            
        }
        objLookupWrap = new LookupsWrapper();
    }
    
    public PageReference saveLookUps(){
        
        try
        {
            system.debug('objLookup=======Picklist__c====='+objLookup.Picklist__c);
            system.debug('objLookup======Picklist2__c======'+objLookup.Picklist2__c);
             system.debug('objLookup============'+objLookupWrap.strUser);
            system.debug('objLookup=====strAccount======='+objLookupWrap.strAccount);
            
            system.debug('strDateVal============'+objLookupWrap.strDateVal);
            system.debug('strPickVal============'+objLookupWrap.strPickVal);
            system.debug('strMultiVal============'+objLookupWrap.strMultiVal); 
            system.debug('intSerialNumVal============'+objLookupWrap.intSerialNumVal);
            system.debug('intNumOfLocVal============'+objLookupWrap.intNumOfLocVal);
            system.debug('strDescVal============'+objLookupWrap.strDescVal);
            system.debug('blnInterestedVal============'+objLookupWrap.blnInterestedVal);
            system.debug('decAmountVal============'+objLookupWrap.decAmountVal);
            system.debug('strPhnVal============'+objLookupWrap.strPhnVal);
            system.debug('decPercentval============'+objLookupWrap.decPercentval);
            system.debug('strOptNameVal============'+objLookupWrap.strOptNameVal);
            system.debug('strEmailVal============'+objLookupWrap.strEmailVal);
            system.debug('strUrlVal============'+objLookupWrap.strUrlVal);
            
            //objLookup.Opportunity__c = String.isBlank(objLookupWrap.strOpp) ? null : objLookupWrap.strOpp ;
            //objLookup.Account__c = String.isBlank(objLookupWrap.strAccount) ? null : objLookupWrap.strAccount ;
            if(objLookup.Contact__c == null)
                objLookup.Contact__c = String.isBlank(objLookupWrap.strContact) ? null : objLookupWrap.strContact ;
            //objLookup.User__c = String.isBlank(objLookupWrap.strUser) ? null : objLookupWrap.strUser ;
            //objLookup.Picklist__c = objLookupWrap.strPickVal;
            objLookup.Amount__c = objLookupWrap.decAmountVal;
            objLookup.Are_you_interested__c = objLookupWrap.blnInterestedVal;
            objLookup.Description__c = objLookupWrap.strDescVal;
            objLookup.Email__c = objLookupWrap.strEmailVal;
           // objLookup.Entry_Date__c = String.isNotBlank(objLookupWrap.strDateVal) ? Date.parse(objLookupWrap.strDateVal) : null;
            objLookup.Num_of_loc__c = objLookupWrap.intNumOfLocVal;
            objLookup.Opt_Name__c = objLookupWrap.strOptNameVal;
            objLookup.Percent__c = objLookupWrap.decPercentval;
            objLookup.Phone__c = objLookupWrap.strPhnVal;
            objLookup.URL__c = objLookupWrap.strUrlVal;
            objLookup.Multiselect__c = GenericInputFieldController.extractSelectedVals(objLookupWrap.strMultiVal);
            
            system.debug('objLookup============'+objLookup);  
            update objLookup;
        }
        catch(Exception e)
        {
            system.debug('===e==='+e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
      
        return null;
        
    }
    
    public class LookupsWrapper
    {
        public String strAccount {get;set;}
        public String strUser {get;set;}
        public String strContact {get;set;}
        public String strOpp {get;set;}
        public String strDateVal    {get;set;}
        public String strPickVal    {get;set;}
        public String strMultiVal    {get;set;}
        public Decimal intSerialNumVal    {get;set;} 
        public Decimal intNumOfLocVal    {get;set;}
        public String strDescVal    {get;set;}
        public Boolean blnInterestedVal    {get;set;}
        public String strUrlVal    {get;set;}
        public Decimal decAmountVal {get;set;}
        public String strPhnVal {get;set;}
        public Decimal decPercentval  {get;set;}
        public String strOptNameVal {get;set;}
        public String strEmailVal {get;set;}
    }
    
}