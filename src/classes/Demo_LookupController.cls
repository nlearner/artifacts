public class Demo_LookupController
{
    public Lookups__c objLookup {get;set;}
    public String strRefOrDTTToBind {get;set;}
    public String strLocale  {get;set;}
    public String strMultiVal {get;set;}
    public String dtType {get;set;}
    public Date dateValue {get;set;}
    public String fieldName {get;set;}
    
    public Demo_LookupController(ApexPages.standardController sc)
    {
        strLocale = UserInfo.getLocale();
        objLookup = new Lookups__c();
        if(sc.getId() != null)
        {
            objLookup = [SELECT Picklist2__c, Id,Name,Account__c,Chkbx_formula__c, 
            Opportunity__c,User__c,SLASerialNumber__c,Picklist__c,Multiselect__c,Contact__c,Contact__r.Name,
                            Account__r.Name,Opportunity__r.Name,User__r.Name,createdDate,
                            Amount__c, Are_you_interested__c, Description__c, Email__c, 
                            Entry_Date__c, Formula__c, Num_of_loc__c, Opt_Name__c,Campaign__c,
                            Percent__c, Phone__c, URL__c, Self_Lookup__c, CLose_Date__C, Start_Time__c, End_Time__c
                            FROM Lookups__c Where Id =: sc.getId() ]; 
            
        }
        
    }
    
    public PageReference saveLookUps(){
        
        try
        {
            objLookup.Multiselect__c = GenericInputFieldController.extractSelectedVals(strMultiVal);
            
            system.debug('objLookup============'+objLookup);  
            upsert objLookup;
        }
        catch(Exception e)
        {
            system.debug('===e==='+e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
      
        return null;
        
    }
    
    public void changedFieldValue()
	{
	    system.debug('=====dtType======'+dtType);
	    system.debug('=====strRefOrDTTToBind======'+strRefOrDTTToBind);
	    system.debug('=====dateValue======'+dateValue);
	    system.debug('=====fieldName======'+fieldName);
	    
	    if(dtType == 'REF')
	    {
	       Id idVal = String.isNotBlank(strRefOrDTTToBind) ? Id.valueOf(strRefOrDTTToBind) : null;
	        objLookup.put(fieldName, idVal);
	    }
	    else if(dtType == 'DT')
	    {
	        objLookup.put(fieldName, dateValue);
	    }
	    else if(dtType == 'DTT')
	    {
	        DateTime dt;
	        List<String>  lstDtSplit = new List<String>();
              if(String.isNotBlank(strRefOrDTTToBind))
                lstDtSplit = strRefOrDTTToBind.split(' ');
              system.debug('===lstDtSplit==='+lstDtSplit);
              if(!lstDtSplit.isEmpty())
              {
                  Date d = Date.parse(lstDtSplit[0]);
                  List<String> lstTimeSplit = lstDtSplit[1].split(':');
                  Time t = Time.newInstance(Integer.valueOf(lstTimeSplit[0]),Integer.valueOf(lstTimeSplit[1]),0,0);
                    system.debug('===Date d===='+d);
                    system.debug('===Time t===='+t);
                    dt = DateTime.newInstance(d,t);
              }
	          objLookup.put(fieldName, dt);
	    }
	}
  
}