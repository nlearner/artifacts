public class PicklistTryController {
    
    public String strAmntType1          {get;set;}
    public String strAmntType2          {get;set;}
    public String strAmntType3          {get;set;}
    public String strAmntType4          {get;set;}
    
    public contact objContact {get;set;}
    
    public PicklistTryController(ApexPages.StandardController stdController) { 
        
        if(stdController.getId() != null)
        {
            objContact = [SELECT Id, Level__c FROM Contact WHERE Id =: stdController.getId()];
        }
        else
        {
            objContact = new Contact(LastName = 'TestCon');
        }
    }
   
    public PageReference CreateContctClone(){
       
       system.debug('**** strPickVal'+strAmntType1);
       system.debug('**** objContact.Level__c'+objContact.Level__c);
       //objContact.Level__c = strAmntType1;
       upsert objContact;
       return null;
    }
}