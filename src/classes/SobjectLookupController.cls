public class SobjectLookupController 
{
	@AuraEnabled
    public static LightningUtility.TabIconDetails getIconDetails(String objectName) {
        return LightningUtility.fetchTabIconDetails(objectName);
    }
    
    public class ALJSWrapper 
    {
        @AuraEnabled
        public String label {get;set;}
        @AuraEnabled
        public String id {get;set;}
        
        public ALJSWrapper(String lb, String strId) {
            
            this.label = lb;
            this.id = strId;
            //this.id = strId.replaceAll('[^a-zA-Z0-9-]','');
        }
    }
    
    //get the record with the search-term on object
    @AuraEnabled
    public static String getSearchedArray(String objName, String searchTerm) { 
      
        List<ALJSWrapper> lstArr = new List<ALJSWrapper>();
        String query = 'SELECT Id, Name FROM '+ objName; 
        if(String.isNotBlank(searchTerm))
        {
            query += ' WHERE Name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\'';
        }
         query += ' ORDER BY Name ASC LIMIT 500';
        for(sObject objsObject : Database.query(query)) {
            lstArr.add(new ALJSWrapper(String.valueOf(objsObject.get('Name')), String.valueOf(objsObject.get('Id'))));
        }  
        return JSON.serialize(lstArr);
    }
}