## Using Lookup lightning component: 
### Artifacts Involved
1. Static Resources:
    * Artifacts / src / staticresources / slds_214
2. Lightning Components / Events:
	* Artifacts / src / aura / svgIcon /
	* Artifacts / src / aura / ALJSErrorEvent /
	* Artifacts / src / aura / Typeahead / 
3. Classes:
	* Artifacts / src / classes / LightningUtility.cls
	* Artifacts / src / classes / SobjectLookupController.cls
	
## Demo component
### Overview:
We have objects called Campsite and Campsite Reservation. Campsite Reservation has MD to Campsite and lookup to User.
Functionality is to clone the Campsite Reservation record.
Artifacts / src / aura / CampsiteReservationClone / has the demo component. 

In CampsiteReservationClone.cmp, let's look into following part of code:
```sh
    <c:Typeahead fieldLabel="Campsite" objName="Campsite__c" 
                                              selectedId="{!v.campsiteReservn.campsiteLkp.id}" 
                                              selectedLabel="{!v.campsiteReservn.campsiteLkp.label}" 
                                              lookupId="campsiteLkp" isRequired="true"
                                              />
```

Typeahead component has following attributes:  
	- Field Label: Label to be displayed as part of form element
	- objName: API name of object for which lookup is for.
	- selectedId: value of lookup/MD field (Eg: objCampsiteReservation.Campsite__c)
	- selectedLabel: value of lookup/MD field's Name (Eg: objCampsiteReservation.Campsite__r.Name)
	- lookupId: Unique Id to lookup, usefull when we fire ALJSErrorEvent from container of this component. setting ALJSErrorEvent.aljsId attribute to lookupId while firing event in Container component + checking aljsId via getParam in typeahead component helps to properly fetch in which component error should be displayed. (Will discuss in detail later)
	- isRequired: Just to display * symbol in markup
![picture](img/Image1.png)